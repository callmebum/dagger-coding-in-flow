package com.example.dagger.model;

public interface Engine {
  void start();
}
