package com.example.dagger;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import com.example.dagger.di.component.ActivityComponent;
import com.example.dagger.di.module.DieselEngineModule;
import com.example.dagger.model.Car;
import javax.inject.Inject;

public class MainActivity extends AppCompatActivity {

  @Inject Car car, anotherCar;
  //private Wheels wheels;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    ActivityComponent activityComponent = ((App) getApplication()).getAppComponent().getActivityComponent(new DieselEngineModule(120));

    activityComponent.inject(this);
    car.drive();
    anotherCar.drive();

  }
}
