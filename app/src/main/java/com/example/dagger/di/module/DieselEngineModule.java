package com.example.dagger.di.module;

import com.example.dagger.model.DieselEngine;
import com.example.dagger.model.Engine;
import com.example.dagger.model.PetrolEngine;
import dagger.Binds;
import dagger.Module;
import dagger.Provides;

@Module
public class DieselEngineModule {
  private int horsePower;

  public DieselEngineModule(int horsePower) {
    this.horsePower = horsePower;
  }

  @Provides
  int horsePower() {
    return horsePower;
  }

  @Provides Engine provideEngine(DieselEngine engine){
    return engine;
  }
}
