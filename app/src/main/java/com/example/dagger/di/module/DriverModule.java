package com.example.dagger.di.module;

import com.example.dagger.model.Driver;
import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import javax.inject.Singleton;

@Module
public abstract class DriverModule {
  @Singleton
  @Provides
  static Driver provideDriver(){
    return new Driver();
  }
}
