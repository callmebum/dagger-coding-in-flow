package com.example.dagger.di.module;

import com.example.dagger.model.Engine;
import com.example.dagger.model.PetrolEngine;
import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import javax.inject.Inject;

@Module
public abstract class PetrolEngineModule {
  @Binds abstract Engine bindEngine(PetrolEngine engine);
}
