package com.example.dagger.di.component;

import com.example.dagger.MainActivity;
import com.example.dagger.di.PerActivity;
import com.example.dagger.di.module.DieselEngineModule;
import com.example.dagger.di.module.WheelsModule;
import com.example.dagger.model.Car;
import dagger.BindsInstance;
import dagger.Component;
import dagger.Subcomponent;
import javax.inject.Named;

@PerActivity
@Subcomponent(modules = { WheelsModule.class, DieselEngineModule.class })
public interface ActivityComponent {

  Car getCar();
  void inject(MainActivity mainActivity);

  /*@Component.Builder
  interface Builder {
    @BindsInstance
    Builder horsePower(@Named("horse power") int horsePower);
    @BindsInstance
    Builder engineCapacity(@Named("engine capacity") int horsePower);
    Builder appComponent(AppComponent appComponent);
    ActivityComponent build();
  }*/
}
