package com.example.dagger.di.component;

import com.example.dagger.di.module.DieselEngineModule;
import com.example.dagger.di.module.DriverModule;
import com.example.dagger.model.Driver;
import dagger.Component;
import javax.inject.Singleton;

@Singleton
@Component(modules = DriverModule.class)
public interface AppComponent {
  ActivityComponent getActivityComponent(DieselEngineModule dieselEngineModule);
}
