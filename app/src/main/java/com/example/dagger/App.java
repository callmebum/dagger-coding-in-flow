package com.example.dagger;

import android.app.Application;
import com.example.dagger.di.component.ActivityComponent;
import com.example.dagger.di.component.AppComponent;
import com.example.dagger.di.component.DaggerAppComponent;

public class App extends Application {
  private AppComponent appComponent;
  @Override public void onCreate() {
    super.onCreate();
    appComponent = DaggerAppComponent.create();
  }

  public AppComponent getAppComponent() {
    return appComponent;
  }
}
